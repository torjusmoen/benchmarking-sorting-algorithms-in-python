import timeit
import copy
import numpy as np
import csv
import matplotlib.pyplot as plt
import pandas as pd
import math
from insertionsort import insertionSort
from bubblesort import bubble
from mergesort import mergeSort
from mergecombine import merge_insertion_combine
from quickcombine import quick_insertion_combine
from quicksort import quicksort
from built_in_sort import pythonsort, numpysort


def make_data(case, listlen):
    """
    Generates random, sorted and reverse sorted data.

    Parameters
    ----------
    case        type of data to be generated
    listlen     length of the list of data

    Returns
    -------
    generated data
    """
    if case == "random":
        # random data from numpy.random
        return np.random.uniform(size=listlen)
    elif case == "sorted":
        # sorted data from zero to the list length
        return list(range(listlen))
    elif case == "reversed":
        # reverse sorted data from the list length to zero
        data = list(range(listlen))
        return data[::-1]


def timer(func, listlen, repeats, case):
    """
    Times the sorting functions.

    Parameters
    ----------
    func        the sorting function to be timed
    listlen     length of the list of data
    repeats     how many times the timer repeats
    case        type of data to be generated

    Returns
    -------
    t_list      list of the time the function used
    """

    rng = np.random.default_rng(12235)
    test_data = make_data(case, listlen)

    clock = timeit.Timer(stmt='sort_func(copy(data))',
                         globals={'sort_func': func,
                                  'data': test_data,
                                  'copy': copy.copy})
    n_ar, t_ar = clock.autorange()

    t_list = np.array(clock.repeat(repeat=repeats, number=n_ar)) / n_ar
    return t_list


def save_file(func_list, listlen_list, repeats, filename, case):
    """
    Saves a csv file with all the results from the timer function.

    Parameters
    ----------
    func_list       list of the sorting functions
    listlen_list    list of the data lengths
    repeats         how many times the timer repeats
    filename        name of the csv file
    case            type of generated data
    """
    # writes the heading
    with open(f"../{filename}.csv", "w+") as file:
        writer = csv.writer(file, delimiter=",")
        writer.writerow(["Function",
                         "Length_of_list", "Time"])

    # writes the data
    for listlen in listlen_list:
        for func in func_list:
            t_list = timer(func, listlen, repeats, case)
            for t in t_list:
                with open(f"../{filename}.csv", "a") as file:
                    writer = csv.writer(file, delimiter=",")
                    t = np.median(t)
                    writer.writerows([[func.__name__, listlen, t]])


def prep_file(old_data, filename, listlen_list, func_list):
    """
    Prepares the csv file for plotting by restructuring the file
    and taking the minimum time for each list length.

    Parameters
    ----------
    old_data        csv file with all the results from the timer function
    filename        name of the new prepared csv file
    listlen_list    list of the data lengths
    func_list       list of the sorting functions
    """
    # writes the heading
    with open(f"../{filename}.csv", "w+") as file:
        writer = csv.writer(file, delimiter=",")
        writer.writerow(
            ["Length_of_list", "insertionSort"])

    old_data = pd.read_csv(f"../{old_data}.csv", delimiter=',')

    # generates the new data
    preped_data = []
    for len in listlen_list:
        preped_data.append(len)
        for func in func_list:
            data = old_data[old_data.Length_of_list == len]
            data = data[data.Function == func.__name__]
            # saves the minimum time from each list length
            t = pd.DataFrame(data, columns=["Time"])
            min_value = np.min(t)
            preped_data.append(min_value.Time)

        # writes the new data
        with open(f"../{filename}.csv", "a") as file:
            writer = csv.writer(file, delimiter=',')
            writer.writerows([preped_data])
            preped_data = []


def plot(prep_filename_csv, plotname, fitted_lines=False):
    """
    Plots the csv file.

    Parameters
    ----------
    prep_filename_csv       name of prepared csv file
    plotname        name of the plot
    """
    df = pd.read_csv(f"../{prep_filename_csv}.csv", delimiter=",")

    y = ["insertionSort"]
        #["insertionSort", "bubble", "mergeSort", "merge_insertion_combine",
         #"quick_insertion_combine", "quicksort", "pythonsort", "numpysort"]
    x = "Length_of_list"

    #color = ["tab:pink", "tab:grey"]
    # plots the file
    ax = df.plot(x, y, marker=".", figsize=(3.6, 3), logx=True, logy=True)
    ax.legend(loc="center left", bbox_to_anchor=(1.0, 0.5)).remove()
    plt.tight_layout(pad=1.16)

    ax.grid(linestyle="--", linewidth=0.5)

    font2 = {'family': 'serif'}
    plt.xlabel("Length of list", fontsize=8, fontdict=font2)
    plt.ylabel("Time [s]", fontsize=8, fontdict=font2)

    if fitted_lines is True:
        values = [2 ** i for i in range(1, 11   )]
        # O(n) line
        line = [10e-7 * i for i in values]
        ax.plot(values, line, linestyle="--", color="tab:olive", alpha=0.7)
        # O(n^2) line
        line = [10e-7 * i**2 for i in values]
        ax.plot(values, line, linestyle="--", color="tab:cyan", alpha=0.7)
        # O(lg(n)) line
        line = [10e-7 * math.log2(i) for i in values]
        ax.plot(values, line, linestyle="--", color="m", alpha=0.7)
        # O(nlg(n)) line
        line = [10e-7 * (i * math.log2(i)) for i in values]
        ax.plot(values, line, linestyle="--", color="tab:grey", alpha=0.7)


    plt.savefig(f"../images/{plotname}.pdf")


def make_all(func_list, listlen_list, case, filename, filename_prep, plotname, fitted_lines=False):
    """
    Runs all functions to generate data, time the algorithms, write the times,
    prepare the csv file and plot.

    Parameters
    ----------
    func_list       list of the sorting functions
    listlen_list    list of the data lengths
    case        type of generated data
    filename        name of the csv file for all the times
    filename_prep       name of the new prepared csv file
    plotname        name of the plot
    """
    save_file(func_list, listlen_list, 5, filename, case)
    prep_file(filename, filename_prep, listlen_list, func_list)
    plot(filename_prep, plotname, fitted_lines)


func_list_test = [insertionSort]
    #[insertionSort, bubble, mergeSort, merge_insertion_combine,
               #   quick_insertion_combine, quicksort, pythonsort, numpysort]

if __name__ == "__main__":
    listlengths = [2 ** i for i in range(1, 11)]
    make_all(func_list_test, listlengths, "random", "data", "data_prep", "TEST", True)
